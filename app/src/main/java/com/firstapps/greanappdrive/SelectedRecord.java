package com.firstapps.greanappdrive;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;;
import android.widget.TextView;
import android.widget.Toast;
import com.firstapps.greanappdrive.Adapters.AdapterSelectedRecord;
import com.firstapps.greanappdrive.Background.ApiCient;
import com.firstapps.greanappdrive.Background.Response.SelectedRecordResponse;
import com.firstapps.greanappdrive.Background.WebServices;
import com.firstapps.greanappdrive.CustomLibray.DataPersistent;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.kaopiz.kprogresshud.KProgressHUD;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by icortes on 27/08/17.
 */

public class SelectedRecord extends AppCompatActivity implements View.OnClickListener{

    private RecyclerView selectedRecordRecyclerView;
    private DataPersistent dataPersistent;
    private KProgressHUD hud;
    public String extrasIdSolicitud;
    private Toolbar toolbar;
    private LinearLayout back_linearLayout;
    private TextView total_tv;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_record_activity);

        back_linearLayout = (LinearLayout) findViewById(R.id.back_linearLayout);
        total_tv = (TextView)findViewById(R.id.total_tv);
        back_linearLayout.setOnClickListener(this);
        dataPersistent= new DataPersistent(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        extrasIdSolicitud = getIntent().getStringExtra("idSolicitud");

        selectedRecordRecyclerView = (RecyclerView) findViewById(R.id.recycler_selected_record_activity);
        selectedRecordRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        get_selected_record(extrasIdSolicitud);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Espere porfavor")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

    }

    public void get_selected_record(String idSolicitud){
        WebServices res = ApiCient.getCliente().create(WebServices.class);

        Log.e("id_solcitud_selected",idSolicitud);

        Call<SelectedRecordResponse> call = res.get_selected_record(dataPersistent.getData(Keys.KEY_TOKEN, ""), idSolicitud);
        call.enqueue(new Callback<SelectedRecordResponse>() {
            @Override
            public void onResponse(Call<SelectedRecordResponse> call, Response<SelectedRecordResponse> response) {
                SelectedRecordResponse selectedRecordResponse = response.body();
                try {
                    if (selectedRecordResponse.status.equals("ok")){
                        AdapterSelectedRecord adapterSelectedRecord = new AdapterSelectedRecord(selectedRecordResponse.request.data,SelectedRecord.this,total_tv);
                        selectedRecordRecyclerView.setAdapter(adapterSelectedRecord);
                    }else {
                        Toast.makeText(SelectedRecord.this, "No se encontraron datos en el historial seleccionado", Toast.LENGTH_LONG).show();
                    }
                }catch (NullPointerException e){
                    Toast.makeText(SelectedRecord.this, "No se encontraron datos en el historial seleccionado", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<SelectedRecordResponse> call, Throwable t) {
                Toast.makeText(SelectedRecord.this,"Servicio temporalmente no disponible",Toast.LENGTH_LONG).show();
                hud.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back_linearLayout:
                onBackPressed();
                break;
        }
    }
}
