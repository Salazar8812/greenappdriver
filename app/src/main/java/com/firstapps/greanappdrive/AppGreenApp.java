package com.firstapps.greanappdrive;

import android.app.Application;
import android.support.multidex.MultiDex;

/**
 * Created by charlssalazar on 30/08/17.
 */

public class AppGreenApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
    }
}
