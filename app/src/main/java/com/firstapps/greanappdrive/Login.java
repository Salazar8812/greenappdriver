package com.firstapps.greanappdrive;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Toast;
import com.firstapps.greanappdrive.Background.ApiCient;
import com.firstapps.greanappdrive.Background.Request.LoginRequest;
import com.firstapps.greanappdrive.Background.Response.LoginResponse;
import com.firstapps.greanappdrive.Background.Services.GPSClass;
import com.firstapps.greanappdrive.Background.WebServices;
import com.firstapps.greanappdrive.CustomLibray.DataPersistent;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.firstapps.greanappdrive.Fragments.MapCollectionFragment;
import com.kaopiz.kprogresshud.KProgressHUD;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by charlssalazar on 12/08/17.
 */

public class Login extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener,CompoundButton.OnCheckedChangeListener {
    private ScrollView main_content;
    private EditText user_edittext,password_editText;
    private Button login_button;
    private KProgressHUD hud;
    private CheckBox rememberAccount_checkBox;
    private Boolean isRemember = false;
    private DataPersistent dataPersistent;
    private LocationManager locationManager;
    public static double latitud;
    public static double longitud;
    private BroadcastReceiver broadcastReceiver;
    public static Activity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        activity = this;
        requestPermission();
        main_content = (ScrollView)findViewById(R.id.main_content);
        user_edittext = (EditText)findViewById(R.id.user_editText);
        password_editText = (EditText)findViewById(R.id.password_editText);
        login_button = (Button)findViewById(R.id.login_button);
        rememberAccount_checkBox = (CheckBox)findViewById(R.id.rememberAccount_checkBox);

        main_content.scrollTo(0, main_content.getBottom());
        dataPersistent = new DataPersistent(this);
        user_edittext.setOnTouchListener(this);
        login_button.setOnClickListener(this);


        user_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                main_content.scrollTo(0, main_content.getBottom());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                main_content.scrollTo(0, main_content.getBottom());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                main_content.scrollTo(0, main_content.getBottom());
            }
        });

        rememberAccount_checkBox.setOnCheckedChangeListener(this);
    }

    public void starLocation(){

        Log.e("StarLocation","Star####");
        if(broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    try {
                        latitud = Double.parseDouble(intent.getExtras().getString("latitud"));
                        longitud = Double.parseDouble(intent.getExtras().getString("longitud"));

                        Log.e("Latitud", "Latitud " + intent.getExtras().getString("latitud"));

                        Log.e("Longitud", "Longitud " + intent.getExtras().getString("longitud"));
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }
            };
        }
        registerReceiver(broadcastReceiver,new IntentFilter("location_update"));

        Intent i =new Intent(getApplicationContext(),GPSClass.class);
        startService(i);
    }

    @Override
    protected void onStart() {
        super.onStart();
        dataPersistent = new DataPersistent(this);
        if(dataPersistent.getData(Keys.KEY_REMEMBER_ACCOUNT,"").equals("true")){
            rememberAccount_checkBox.setChecked(true);
            user_edittext.setText(dataPersistent.getData(Keys.KEY_USER,""));
            password_editText.setText(dataPersistent.getData(Keys.KEY_PASSWORD,""));

        }else{
            rememberAccount_checkBox.setChecked(false);
        }
    }

    public void gpsCheckStatus(){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent,0);
        }else{
            Log.e("iniciar","startLocation");
            starLocation();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("OnActivityReult","Catch Result");
        starLocation();
    }

    public void get_login() {
        WebServices res = ApiCient.getCliente().create(WebServices.class);

        LoginRequest request= new LoginRequest();
        request.usuario = user_edittext.getText().toString();
        request.password = password_editText.getText().toString();

        Call<LoginResponse> call = res.get_login(request);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();
                try {
                    if (loginResponse.status.equals("ok")){
                        Log.e("#################",loginResponse.token);
                        dataPersistent.saveData(Keys.KEY_TOKEN,loginResponse.token);
                        dataPersistent.saveData(Keys.KEY_ID,loginResponse.usuario.id);
                        dataPersistent.saveData(Keys.KEY_USER_NICK_NAME,loginResponse.usuario.usuario);
                        dataPersistent.saveData(Keys.KEY_EMAIL,loginResponse.usuario.email);
                        dataPersistent.saveData(Keys.KEY_NAME,loginResponse.usuario.nombres);
                        dataPersistent.saveData(Keys.KEY_LASTNAME,loginResponse.usuario.apellidos);
                        dataPersistent.saveData(Keys.KEY_PHONE,loginResponse.usuario.telefono);
                        dataPersistent.saveData(Keys.KEY_USER_TYPE,loginResponse.usuario.tipo_usuario);
                        dataPersistent.saveData(Keys.KEY_URL_PHOTO,loginResponse.usuario.url_foto);
                        dataPersistent.saveData(Keys.KEY_ENTERPRISE_NAME,loginResponse.usuario.nombre_empresa);
                        dataPersistent.saveData(Keys.KEY_ENTERPRISE_PHONE,loginResponse.usuario.telefono_empresa);
                        dataPersistent.saveData(Keys.KEY_CLABE,loginResponse.usuario.clabe_interbancaria);
                        dataPersistent.saveData(Keys.KEY_STATUS,loginResponse.usuario.estatus);
                        dataPersistent.saveData(Keys.KEY_ID_STATE,loginResponse.usuario.id_estado);
                        dataPersistent.saveData(Keys.KEY_NAME_STATE,loginResponse.usuario.nombre_estado);
                        dataPersistent.saveData(Keys.KEY_ID_CITY,loginResponse.usuario.id_ciudad);
                        dataPersistent.saveData(Keys.KEY_NAME_CITY,loginResponse.usuario.nombre_ciudad);
                        dataPersistent.saveData(Keys.KEY_STREET,loginResponse.usuario.calle);
                        dataPersistent.saveData(Keys.KEY_COLONY,loginResponse.usuario.colonia);
                        dataPersistent.saveData(Keys.KEY_ZIP_CODE,loginResponse.usuario.codigo_postal);
                        dataPersistent.saveData(Keys.KEY_DOWN_DATE,loginResponse.usuario.fecha_baja);
                        dataPersistent.saveData(Keys.KEY_NUMBER_INT,loginResponse.usuario.num_interior);
                        dataPersistent.saveData(Keys.KEY_NUMBER_EXT,loginResponse.usuario.num_exterior);
                        dataPersistent.saveData(Keys.KEY_LAT,loginResponse.usuario.latitud);
                        dataPersistent.saveData(Keys.KEY_LON,loginResponse.usuario.longitud);
                        dataPersistent.saveData(Keys.KEY_ID_FIREBASE,loginResponse.usuario.id_firebase);
                        dataPersistent.saveData(Keys.KEY_LEAD,loginResponse.usuario.responsable);
                        dataPersistent.saveData(Keys.KEY_REGISTER_DATE,loginResponse.usuario.fecha_reigistro);
                        dataPersistent.saveData(Keys.KEY_TYPE_CLIENT,loginResponse.usuario.tipo_cliente);
                        dataPersistent.saveData(Keys.KEY_BANK_NAME,loginResponse.usuario.nombre_banco);
                        dataPersistent.saveData(Keys.KEY_TOTAL_FINISH,String.valueOf(loginResponse.finalizadas.total));
                        dataPersistent.saveData(Keys.KEY_TOTAL_PENDING,String.valueOf(loginResponse.pendientes.total));
                        dataPersistent.saveData(Keys.KEY_TOTAL_TRANSFER,String.valueOf(loginResponse.totales.total));
                        dataPersistent.saveData(Keys.KEY_TOTOAL_TRANSFER_MONEY,String.valueOf(loginResponse.totales.dinero));
                        startActivity(new Intent(Login.this, MainApp.class));
                    } else {
                        Toast.makeText(Login.this, "Correo electrónico o contraseña incorrecta", Toast.LENGTH_LONG).show();
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                    Toast.makeText(Login.this, "Correo electrónico o contraseña incorrecta", Toast.LENGTH_LONG).show();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(Login.this,"Servicio temporalmente no disponible",Toast.LENGTH_LONG).show();
                hud.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button:
                if(user_edittext.getText().toString().equals("") || password_editText.toString().equals("")){
                    Toast.makeText(this,"Es necesario llenar los campos",Toast.LENGTH_LONG).show();
                }else{
                    if(isRemember){
                        dataPersistent.saveData(Keys.KEY_USER,user_edittext.getText().toString());
                        dataPersistent.saveData(Keys.KEY_PASSWORD,password_editText.getText().toString());
                    }
                   hud = KProgressHUD.create(Login.this)
                            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                            .setLabel("Espere porfavor")
                            .setCancellable(false)
                            .setAnimationSpeed(2)
                            .setDimAmount(0.5f)
                            .show();

                    get_login();
                }
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Permisos","Permisos Otorgados");
                    gpsCheckStatus();
                } else {

                }
        }
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(Login.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(Login.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_COARSE_LOCATION}, 1); //Any number can be used    }
        }else{
            Log.d("Permisos","Permisos Otorgados");
            gpsCheckStatus();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        main_content.scrollTo(0, main_content.getBottom());
        return false;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        dataPersistent = new DataPersistent(this);
        if(b){
            isRemember=true;
            dataPersistent.saveData(Keys.KEY_REMEMBER_ACCOUNT,"true");
        }else{
            isRemember=false;
            dataPersistent.saveData(Keys.KEY_REMEMBER_ACCOUNT,"false");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
    }

}
