package com.firstapps.greanappdrive.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.firstapps.greanappdrive.Adapters.AdapterDates;
import com.firstapps.greanappdrive.Background.ApiCient;
import com.firstapps.greanappdrive.Background.Request.MarkersRequest;
import com.firstapps.greanappdrive.Background.Request.TakAnAppointmentRequest;
import com.firstapps.greanappdrive.Background.Response.MarkersResponse;
import com.firstapps.greanappdrive.Background.Response.TakAnAppointmentResponse;
import com.firstapps.greanappdrive.Background.WebServices;
import com.firstapps.greanappdrive.CustomLibray.DataPersistent;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.firstapps.greanappdrive.DetailCollectionMap;
import com.firstapps.greanappdrive.Fragments.DateFragment;
import com.firstapps.greanappdrive.MainApp;
import com.firstapps.greanappdrive.Model.SaveDate;
import com.firstapps.greanappdrive.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by charlssalazar on 27/08/17.
 */

public class AdvisoryDates_Dialog extends DialogFragment implements View.OnClickListener{
    private Button accept_button;
    private Button cancel_button;
    private TextView titleDialog_textView;
    private TextView bodyDialog_textView;
    private String titleDilog;
    private String bodyDialog;
    private String titleButtonAcceptDialog;
    private String titleButtonCancelDialog;
    private String idSolicitud;
    private String dialogType;
    private String typeAction;
    private DataPersistent dataPersistent;
    private KProgressHUD hud;
    private int position = 0;
    private SaveDate saveDate;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_dates_fragment, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        accept_button = (Button) view.findViewById(R.id.accept_button);
        cancel_button = (Button) view.findViewById(R.id.cancel_button);
        titleDialog_textView =(TextView) view.findViewById(R.id.titleDialog_textView);
        bodyDialog_textView = (TextView) view.findViewById(R.id.bodyDialod_textView);

        titleDilog = getArguments().getString("titleDialog");
        bodyDialog = getArguments().getString("bodyDialog");

        typeAction = getArguments().getString("typeAction");

        titleButtonAcceptDialog = getArguments().getString("titleButtonAcceptDialog");
        titleButtonCancelDialog = getArguments().getString("titleButtonCancelDialog");
        try {
            position = getArguments().getInt("position");
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        idSolicitud = getArguments().getString("idSolcitud");

        dialogType = getArguments().getString(Keys.KEY_TYPE_DIALOG);

        saveDate = new SaveDate();
        saveDate.id_solicitud = idSolicitud;
        saveDate.position = String.valueOf(position);

        titleDialog_textView.setText(titleDilog);
        bodyDialog_textView.setText(bodyDialog);
        accept_button.setText(titleButtonAcceptDialog);
        cancel_button.setText(titleButtonCancelDialog);

        accept_button.setOnClickListener(this);
        cancel_button.setOnClickListener(this);

        dataPersistent = new DataPersistent(getActivity());

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.accept_button:
                detectDialog();
                break;

            case R.id.cancel_button:
                detectCancel();
                break;
        }
    }

    public void detectCancel(){
        switch (dialogType){
            case "dateDialog":
                dismiss();
                break;

            case "collectionMapDialog":
                dismiss();
                break;
        }
    }

    public void detectDialog(){
        switch (dialogType){
            case "dateDialog":
                showProgress();
                if(typeAction.equals("Apartar")){
                    takeDate();
                }else{
                    cancelDate();
                }
                dismiss();
                break;

            case "collectionMapDialog":
                showProgress();
                takeRequest();
                dismiss();
                break;
        }
    }


    public void showProgress(){
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Espere porfavor")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public void takeDate(){
        WebServices res = ApiCient.getCliente().create(WebServices.class);

        Call<TakAnAppointmentResponse> call = res.take_date(dataPersistent.getData(Keys.KEY_TOKEN, ""), idSolicitud);
        call.enqueue(new Callback<TakAnAppointmentResponse>() {
            @Override
            public void onResponse(Call<TakAnAppointmentResponse> call, Response<TakAnAppointmentResponse> response) {
                TakAnAppointmentResponse requestMarkersResponse = response.body();
                try {
                    if (requestMarkersResponse.status.equals("ok")) {
                        DateFragment.get_store();
                        AdapterDates.iconImage.setImageResource(R.drawable.ic_minus);
                        Toast.makeText(getActivity(),"Cita tomada correctamente",Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Ocurrio un error al tomar la solcitud", Toast.LENGTH_LONG).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                dismiss();
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<TakAnAppointmentResponse> call, Throwable t) {
                hud.dismiss();
                dismiss();
            }
        });
    }


    public void cancelDate(){
        WebServices res = ApiCient.getCliente().create(WebServices.class);

        Call<TakAnAppointmentResponse> call = res.cancel_date(dataPersistent.getData(Keys.KEY_TOKEN, ""), idSolicitud);
        call.enqueue(new Callback<TakAnAppointmentResponse>() {
            @Override
            public void onResponse(Call<TakAnAppointmentResponse> call, Response<TakAnAppointmentResponse> response) {
                TakAnAppointmentResponse requestMarkersResponse = response.body();
                try {
                    if (requestMarkersResponse.status.equals("ok")) {
                        DateFragment.get_store();
                        AdapterDates.iconImage.setImageResource(R.drawable.ic_add_plus);
                        Toast.makeText(getActivity(),"Cita cancelada correctamente",Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getActivity(), "Ocurrio un error al tomar la solcitud", Toast.LENGTH_LONG).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                dismiss();
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<TakAnAppointmentResponse> call, Throwable t) {
                hud.dismiss();
                dismiss();
            }
        });
    }

    public void takeRequest(){
        WebServices res = ApiCient.getCliente().create(WebServices.class);
        TakAnAppointmentRequest takAnAppointmentRequest = new TakAnAppointmentRequest();
        takAnAppointmentRequest.solicitud = idSolicitud;

        Call<TakAnAppointmentResponse> call = res.take_request(dataPersistent.getData(Keys.KEY_TOKEN, ""), takAnAppointmentRequest);
        call.enqueue(new Callback<TakAnAppointmentResponse>() {
            @Override
            public void onResponse(Call<TakAnAppointmentResponse> call, Response<TakAnAppointmentResponse> response) {
                TakAnAppointmentResponse requestMarkersResponse = response.body();
                try {
                    if (requestMarkersResponse.status.equals("ok")) {
                        Intent intent = new Intent(MainApp.activity, DetailCollectionMap.class);
                        intent.putExtra("idSolicitud",idSolicitud);
                        MainApp.activity.startActivityForResult(intent,0);
                    } else {
                        Toast.makeText(getActivity(), "Ocurrio un error al tomar la solcitud", Toast.LENGTH_LONG).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                hud.dismiss();
                dismiss();
            }

            @Override
            public void onFailure(Call<TakAnAppointmentResponse> call, Throwable t) {
                hud.dismiss();
                dismiss();
            }
        });
    }

}
