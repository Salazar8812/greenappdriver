package com.firstapps.greanappdrive.Fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firstapps.greanappdrive.Adapters.AdapterDates;
import com.firstapps.greanappdrive.Adapters.AdapterStore;
import com.firstapps.greanappdrive.Background.ApiCient;
import com.firstapps.greanappdrive.Background.Request.DateRequest;
import com.firstapps.greanappdrive.Background.Response.DateResponse;
import com.firstapps.greanappdrive.Background.Response.StoresResponse;
import com.firstapps.greanappdrive.Background.WebServices;
import com.firstapps.greanappdrive.CustomLibray.DataPersistent;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.firstapps.greanappdrive.Model.Stores;
import com.firstapps.greanappdrive.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by charlssalazar on 19/08/17.
 */

public class DateFragment extends Fragment implements Spinner.OnItemSelectedListener,Spinner.OnTouchListener{

    private static RecyclerView storesRecyclerView;
    private static Spinner idAlmacenSpinner;
    private static DataPersistent dataPersistent;
    private static KProgressHUD hud;
    private TextView street_textView;
    private TextView colony_textView;
    private TextView zipCode_textView;
    private TextView number_textView;
    private TextView overture_textView;
    private TextView close_textView;
    private TextView maxTime_textView;
    private TextView minTime_textView;
    private static Activity activity;
    private boolean isEnterFirst = true;
    private static AdapterDates adapterDates;
    private static DateResponse dateResponse;
    private boolean isEnterFirstDate = true;
    private static LinearLayout thumnail_greenapp;
    private ImageView logo_dates_imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         View v = inflater.inflate(R.layout.date_fragment, container, false);
        storesRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_date_fragment);
        idAlmacenSpinner = (Spinner) v.findViewById(R.id.date_frag_id_almacen);

        street_textView =  (TextView) v.findViewById(R.id.street_textView);
        colony_textView =  (TextView) v.findViewById(R.id.colony_textView);
        zipCode_textView =  (TextView) v.findViewById(R.id.zipCode_textView);
        number_textView =  (TextView) v.findViewById(R.id.number_textView);
        overture_textView =  (TextView) v.findViewById(R.id.overture_textView);
        close_textView =  (TextView) v.findViewById(R.id.close_textView);
        maxTime_textView =  (TextView) v.findViewById(R.id.maxTime_textView);
        minTime_textView =  (TextView) v.findViewById(R.id.minTime_textView);
        thumnail_greenapp = (LinearLayout) v.findViewById(R.id.thumnail_greenapp);
        logo_dates_imageView = (ImageView) v.findViewById(R.id.logo_dates_imageView);

        Glide.with(getActivity()).load(R.drawable.ic_greenapp_blue).into(logo_dates_imageView);

        activity = getActivity();
        dataPersistent= new DataPersistent(getActivity());
        storesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        idAlmacenSpinner.setOnItemSelectedListener(this);
        idAlmacenSpinner.setOnTouchListener(this);

        return v;
    }

    public static void get_store(){
        WebServices res = ApiCient.getCliente().create(WebServices.class);
        Call<StoresResponse> call = res.get_stores(dataPersistent.getData(Keys.KEY_TOKEN,""));
        call.enqueue(new Callback<StoresResponse>() {
            @Override
            public void onResponse(Call<StoresResponse> call, Response<StoresResponse> response) {
                StoresResponse storesResponse = response.body();
                try {
                    if (storesResponse.status.equals("ok")){
                        AdapterStore adapter = new AdapterStore(activity, R.layout.store_spinner_item, storesResponse.stores,activity);
                        idAlmacenSpinner.setAdapter(adapter);
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
                //hud.dismiss();
            }

            @Override
            public void onFailure(Call<StoresResponse> call, Throwable t) {
                //hud.dismiss();
            }
        });
    }

    public static void get_dates(String idAlmacen){
        showProgress();
        thumnail_greenapp.setVisibility(View.GONE);
        WebServices res = ApiCient.getCliente().create(WebServices.class);
        final DateRequest dateRequest = new DateRequest();
        dateRequest.almacen = idAlmacen;

        Call<DateResponse> call = res.get_dates(dataPersistent.getData(Keys.KEY_TOKEN,""),dateRequest);
        call.enqueue(new Callback<DateResponse>() {
            @Override
            public void onResponse(Call<DateResponse> call, Response<DateResponse> response) {
                dateResponse = response.body();
                try {
                    if (dateResponse.status.equals("ok")){
                        adapterDates = new AdapterDates(activity,dateResponse.dates);
                        storesRecyclerView.setAdapter(adapterDates);
                    }else {
                        Toast.makeText(activity, "No se encontro ninguna cita", Toast.LENGTH_LONG).show();
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                    adapterDates.notifyDataSetChanged();
                    storesRecyclerView.removeAllViewsInLayout();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<DateResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        /*if(isEnterFirstDate) {
            isEnterFirstDate=false;
        }else{*/
            Stores stores = (Stores) adapterView.getItemAtPosition(i);
            get_dates(stores.id);
            street_textView.setText(stores.calle);
            colony_textView.setText(stores.colonia);
            zipCode_textView.setText(stores.codigo_postal);
            number_textView.setText(stores.numero_exterior);
            overture_textView.setText(stores.hora_apertura);
            close_textView.setText(stores.hora_cierre);
            maxTime_textView.setText(stores.tiempo_cita_hora);
            minTime_textView.setText(stores.tiempo_cita_min);
        //}
    }

    public static void showProgress(){
        hud = KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Espere porfavor")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(isEnterFirst) {
            get_store();
            isEnterFirst=false;
        }
        return false;
    }
}
