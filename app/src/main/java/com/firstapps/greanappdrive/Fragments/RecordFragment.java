package com.firstapps.greanappdrive.Fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firstapps.greanappdrive.Adapters.AdapterRecord;
import com.firstapps.greanappdrive.Background.ApiCient;
import com.firstapps.greanappdrive.Background.Request.RecordRequest;
import com.firstapps.greanappdrive.Background.Response.RecordResponse;
import com.firstapps.greanappdrive.Background.WebServices;
import com.firstapps.greanappdrive.CustomLibray.DataPersistent;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.firstapps.greanappdrive.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by charlssalazar on 19/08/17.
 */

public class RecordFragment extends Fragment {

    private static RecyclerView recordRecyclerView;
    private static DataPersistent dataPersistent;
    private static KProgressHUD hud;
    private static Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.record_fragment, container, false);
        activity = getActivity();
        dataPersistent= new DataPersistent(getActivity());
        recordRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_record_fragment);
        recordRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        get_record("1");

        return v;
    }

    public static void showProgress(){
        hud = KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Espere porfavor")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public static void get_record(String page){
        showProgress();
        WebServices res = ApiCient.getCliente().create(WebServices.class);
        final RecordRequest recordRequest = new RecordRequest();
        recordRequest.page =  page;
        Call<RecordResponse> call = res.get_record(dataPersistent.getData(Keys.KEY_TOKEN, ""), recordRequest);
        call.enqueue(new Callback<RecordResponse>() {
            @Override
            public void onResponse(Call<RecordResponse> call, Response<RecordResponse> response) {
                RecordResponse recordResponse = response.body();
                try {
                    if (recordResponse.status.equals("ok")){
                        AdapterRecord adapterRecord = new AdapterRecord(activity, recordResponse.request.data);
                        recordRecyclerView.setAdapter(adapterRecord);
                    }else {
                        Toast.makeText(activity , "No se encontro ningun historial", Toast.LENGTH_LONG).show();
                    }
                }catch (NullPointerException e){
                    Toast.makeText(activity, "No se encontro ningun historial", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<RecordResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }
}
