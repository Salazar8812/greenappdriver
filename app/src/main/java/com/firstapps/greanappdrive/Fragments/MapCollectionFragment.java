package com.firstapps.greanappdrive.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.firstapps.greanappdrive.Background.ApiCient;
import com.firstapps.greanappdrive.Background.Request.MarkersRequest;
import com.firstapps.greanappdrive.Background.Request.TakAnAppointmentRequest;
import com.firstapps.greanappdrive.Background.Response.MarkersResponse;
import com.firstapps.greanappdrive.Background.Response.TakAnAppointmentResponse;
import com.firstapps.greanappdrive.Background.WebServices;
import com.firstapps.greanappdrive.CustomLibray.DataPersistent;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.firstapps.greanappdrive.CustomLibray.MapInfoLayout;
import com.firstapps.greanappdrive.DetailCollectionMap;
import com.firstapps.greanappdrive.Dialogs.AdvisoryDates_Dialog;
import com.firstapps.greanappdrive.Login;
import com.firstapps.greanappdrive.MainApp;
import com.firstapps.greanappdrive.Model.RequestMarkers;
import com.firstapps.greanappdrive.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.wunderlist.slidinglayer.SlidingLayer;

import java.text.DecimalFormat;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by charlssalazar on 18/08/17.
 */

public class MapCollectionFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener,View.OnClickListener {
    private static GoogleMap map;
    private SlidingLayer slidingLayer;
    private static DataPersistent dataPersistent;
    private static KProgressHUD hud;
    private TextView detail_enterprise_text_view;
    private TextView title_Enterprise_textView;
    private Button collect_Button;
    private String titleDialog= "";
    private String bodyDialog = "";
    private static Activity activity;
    String[] splitContentMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.map_collection_fragment, container, false);
        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);
        activity = getActivity();

        detail_enterprise_text_view = (TextView) v.findViewById(R.id.detail_enterprise_text_view);
        title_Enterprise_textView = (TextView) v.findViewById(R.id.title_Enterprise_textView);
        collect_Button = (Button) v.findViewById(R.id.collect_Button);
        slidingLayer = (SlidingLayer) v.findViewById(R.id.slidingLayer1);
        slidingLayer.setShadowSizeRes(R.dimen.shadow_size);
        slidingLayer.setOffsetDistanceRes(R.dimen.offset_distance);
        slidingLayer.setPreviewOffsetDistanceRes(R.dimen.preview_offset_distance);
        slidingLayer.setStickTo(SlidingLayer.STICK_TO_BOTTOM);
        slidingLayer.setChangeStateOnTap(false);

        dataPersistent = new DataPersistent(getActivity());

        initRequestFromPreference();

        get_request_markers();

        collect_Button.setOnClickListener(this);

        return v;
    }

    public static void showProgress(){
        hud = KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Espere porfavor")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public void initRequestFromPreference(){
        if(!dataPersistent.getData(Keys.KEY_ID_SOLCITUD,"").equals("")){
            WebServices res = ApiCient.getCliente().create(WebServices.class);
            TakAnAppointmentRequest takAnAppointmentRequest = new TakAnAppointmentRequest();
            takAnAppointmentRequest.solicitud = dataPersistent.getData(Keys.KEY_ID_SOLCITUD,"");

            Call<TakAnAppointmentResponse> call = res.take_request(dataPersistent.getData(Keys.KEY_TOKEN, ""), takAnAppointmentRequest);
            call.enqueue(new Callback<TakAnAppointmentResponse>() {
                @Override
                public void onResponse(Call<TakAnAppointmentResponse> call, Response<TakAnAppointmentResponse> response) {
                    TakAnAppointmentResponse requestMarkersResponse = response.body();
                    try {
                        if (requestMarkersResponse.status.equals("ok")) {
                            Intent intent = new Intent(MainApp.activity, DetailCollectionMap.class);
                            intent.putExtra("idSolicitud",dataPersistent.getData(Keys.KEY_ID_SOLCITUD,""));
                            MainApp.activity.startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "Ocurrio un error al tomar la solcitud", Toast.LENGTH_LONG).show();
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    hud.dismiss();
                }

                @Override
                public void onFailure(Call<TakAnAppointmentResponse> call, Throwable t) {
                    hud.dismiss();
                }
            });
        }
    }

    public static void get_request_markers() {
        showProgress();
        WebServices res = ApiCient.getCliente().create(WebServices.class);
        MarkersRequest dateRequest = new MarkersRequest();
        dateRequest.latitud = String.valueOf(Login.latitud);
        dateRequest.longitud = String.valueOf(Login.longitud);
        dateRequest.distancia = "1000";

        Call<MarkersResponse> call = res.get_solicitudes(dataPersistent.getData(Keys.KEY_TOKEN, ""), dateRequest);
        call.enqueue(new Callback<MarkersResponse>() {
            @Override
            public void onResponse(Call<MarkersResponse> call, Response<MarkersResponse> response) {
                MarkersResponse requestMarkersResponse = response.body();
                try {
                    if (requestMarkersResponse.estatus.equals("ok")) {
                        draw_markers_in_map(requestMarkersResponse.list_request_markers);
                    } else {
                        Toast.makeText(activity, "No se encontraron solicitudes", Toast.LENGTH_LONG).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<MarkersResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnInfoWindowClickListener(this);
        map.setInfoWindowAdapter(new MapInfoLayout(getActivity().getLayoutInflater()));
    }

    public static void draw_markers_in_map(List<RequestMarkers> list_request_markers) {
        try {
            map.clear();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        for (RequestMarkers item : list_request_markers) {
            try {
                LatLng ubicacion_propiedad = new LatLng(Double.parseDouble(item.latitud), Double.parseDouble(item.longitud));
                Marker marcador = map.addMarker(new MarkerOptions()
                        .position(ubicacion_propiedad)
                        .title(item.nombres)
                        .snippet("Nº:" + item.clave + ", " + new DecimalFormat("##.##").format(item.distance) + "Km , " + item.total_kg + "Kg")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_market_red)));
            }catch (NumberFormatException | NullPointerException  e){
                    e.printStackTrace();
            }
        }

        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Login.latitud, Login.longitud), 7.5f));
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        titleDialog = marker.getTitle();
        bodyDialog = marker.getSnippet();
        splitContentMap = bodyDialog.split(",");
        callDialog(parseCad(bodyDialog));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.collect_Button:

                break;
        }
    }

    public String parseCad(String bodyD){
        String cadena="";
        String aux="";

        for(int i=0;i<bodyD.length();i++){
            aux = String.valueOf(bodyD.charAt(i));
            if(!aux.equals(",")){
                cadena = cadena + aux;
            }else{
                i = bodyD.length();
            }
        }

        cadena = getClave(cadena);



        return cadena;
    }

    public String getClave(String resultCad){
        String clave="";
        String aux;
        for(int i=0;i<resultCad.length();i++){
            aux = String.valueOf(resultCad.charAt(i));
            if(!aux.equals("º") && !aux.equals("N") && !aux.equals(":")){
                clave = clave + aux;
            }
        }

        Log.e("#### Parse Cadena ####",clave);
        return clave;
    }

    public void callDialog(String clave){
        dataPersistent.saveData(Keys.KEY_ID_SOLCITUD,clave);
        AdvisoryDates_Dialog dialog= new AdvisoryDates_Dialog();
        Bundle b= new Bundle();
        b.putString("titleDialog", titleDialog);
        b.putString("bodyDialog","Nº Solicitud: "+clave+", Distancia: "+splitContentMap[1]+ ", Kilogramos: "+splitContentMap[2]);
        b.putString("titleButtonAcceptDialog","Recolectar");
        b.putString("titleButtonCancelDialog", "Cancelar");
        b.putString("idSolcitud",clave);
        b.putString(Keys.KEY_TYPE_DIALOG,"collectionMapDialog");
        dialog.setArguments(b);
        dialog.show(getActivity().getFragmentManager(),null);
    }
}


