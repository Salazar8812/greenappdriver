package com.firstapps.greanappdrive.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.firstapps.greanappdrive.Background.Response.LoginResponse;
import com.firstapps.greanappdrive.CustomLibray.DataPersistent;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.firstapps.greanappdrive.Model.User;
import com.firstapps.greanappdrive.R;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

/**
 * Created by charlssalazar on 19/08/17.
 */

public class PerfilFragment extends Fragment {
    private TextView perfil_frag_name;
    private TextView perfil_frag_last_name;
    private TextView perfil_frag_email;
    private TextView pending_textView;
    private TextView transfer_textView;
    private TextView finish_textView;
    private EditText city_editText;
    private EditText delegation_editText;
    private EditText street_editText;
    private EditText numberExt_editText;
    private EditText numberInt_editText;
    private EditText colony_editText;
    private EditText phone_editText;
    private EditText institutionBank_editText;
    private ImageView imgFotoPerfil;
    private DataPersistent dataPersistent;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.perfil_fragment, container, false);

        dataPersistent = new DataPersistent(getActivity());
        perfil_frag_name = (TextView) v.findViewById(R.id.perfil_frag_name);
        perfil_frag_last_name = (TextView) v.findViewById(R.id.perfil_frag_last_name);
        perfil_frag_email = (TextView) v.findViewById(R.id.perfil_frag_email);
        pending_textView = (TextView) v.findViewById(R.id.pending_textView);
        transfer_textView = (TextView) v.findViewById(R.id.transfer_textView);
        finish_textView = (TextView) v.findViewById(R.id.finish_textView);

        city_editText = (EditText) v.findViewById(R.id.city_editText);
        delegation_editText = (EditText) v.findViewById(R.id.delegation_editText);
        street_editText = (EditText) v.findViewById(R.id.street_editText);
        numberExt_editText = (EditText) v.findViewById(R.id.numberExt_editText);
        numberInt_editText = (EditText) v.findViewById(R.id.numberInt_editText);
        colony_editText = (EditText) v.findViewById(R.id.colony_editText);
        phone_editText = (EditText) v.findViewById(R.id.phone_editText);
        institutionBank_editText = (EditText) v.findViewById(R.id.institutionBank_editText);
        imgFotoPerfil = (ImageView) v.findViewById(R.id.imgFotoPerfil);

        perfil_frag_name.setText(dataPersistent.getData(Keys.KEY_NAME,""));
        perfil_frag_last_name.setText(dataPersistent.getData(Keys.KEY_LASTNAME,""));
        perfil_frag_email.setText(dataPersistent.getData(Keys.KEY_EMAIL,""));
        pending_textView.setText(dataPersistent.getData(Keys.KEY_TOTAL_PENDING,""));
        transfer_textView.setText(String.format("$%s", new DecimalFormat("##.##").format(Double.parseDouble(dataPersistent.getData(Keys.KEY_TOTOAL_TRANSFER_MONEY, "")))));
        city_editText.setText(dataPersistent.getData(Keys.KEY_NAME_CITY,""));
        delegation_editText.setText(dataPersistent.getData(Keys.KEY_NAME_CITY,""));
        street_editText.setText(dataPersistent.getData(Keys.KEY_STREET,""));
        numberExt_editText.setText(dataPersistent.getData(Keys.KEY_NUMBER_EXT,""));
        numberInt_editText.setText(dataPersistent.getData(Keys.KEY_NUMBER_INT,""));
        colony_editText.setText(dataPersistent.getData(Keys.KEY_COLONY,""));
        phone_editText.setText(dataPersistent.getData(Keys.KEY_PHONE,""));
        institutionBank_editText.setText(dataPersistent.getData(Keys.KEY_BANK_NAME,""));

        return v;
    }
}
