package com.firstapps.greanappdrive.Background.Request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class LoginRequest {
    @SerializedName("usuario")
    public String usuario;

    @SerializedName("password")
    public String password;
}
