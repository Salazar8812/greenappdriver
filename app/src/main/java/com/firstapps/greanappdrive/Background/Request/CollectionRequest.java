package com.firstapps.greanappdrive.Background.Request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by icortes on 29/08/17.
 */

public class CollectionRequest {

    @SerializedName("id_solicitud")
    public String idSolicitud;
}
