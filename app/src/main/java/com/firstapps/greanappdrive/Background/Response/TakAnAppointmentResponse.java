package com.firstapps.greanappdrive.Background.Response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by icortes on 27/08/17.
 */

public class TakAnAppointmentResponse {
    @SerializedName("estatus")
    public String status;
    @SerializedName("login")
    public String login;
    @SerializedName("mensaje")
    public String message;
}
