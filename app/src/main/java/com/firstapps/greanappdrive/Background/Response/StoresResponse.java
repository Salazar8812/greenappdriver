package com.firstapps.greanappdrive.Background.Response;

import com.firstapps.greanappdrive.Model.Stores;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by icortes on 23/08/17.
 */

public class StoresResponse {

    @SerializedName("estatus")
    public String status;

    @SerializedName("almacenes")
    public ArrayList<Stores> stores = new ArrayList<>();
}
