package com.firstapps.greanappdrive.Background.Request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by icortes on 27/08/17.
 */

public class RecordRequest {

    @SerializedName("page")
    public String page;
}
