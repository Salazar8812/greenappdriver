package com.firstapps.greanappdrive.Background.Response;

import com.firstapps.greanappdrive.Model.DetailGeneric;
import com.firstapps.greanappdrive.Model.HeaderGeneric;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charlssalazar on 03/09/17.
 */

public class DetailHeaderResponse {
    @SerializedName("estatus")
    public String estatus;

    @SerializedName("cabecera_solicitud")
    public HeaderGeneric cabecera_solicitud;

    @SerializedName("cabecera_recepcion")
    public HeaderGeneric cabecera_recepcion;

    @SerializedName("detalle_solicitud")
    public DetailGeneric detalle_solicitud;

    @SerializedName("detalle_recepcion")
    public DetailGeneric detalle_recepcion;


}
