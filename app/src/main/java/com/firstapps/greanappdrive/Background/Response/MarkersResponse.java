package com.firstapps.greanappdrive.Background.Response;

import com.firstapps.greanappdrive.Model.RequestMarkers;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charlssalazar on 30/08/17.
 */

public class MarkersResponse {

    @SerializedName("estatus")
    public String estatus;

    @SerializedName("solicitudes")
    public List<RequestMarkers> list_request_markers = new ArrayList<>();

}
