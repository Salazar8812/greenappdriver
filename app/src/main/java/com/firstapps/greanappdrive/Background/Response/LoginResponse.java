package com.firstapps.greanappdrive.Background.Response;

import com.firstapps.greanappdrive.Model.Error;
import com.firstapps.greanappdrive.Model.GenericStatusDate;
import com.firstapps.greanappdrive.Model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class LoginResponse {
    @SerializedName("status")
    public String status;

    @SerializedName("token")
    public String token;

    @SerializedName("usuario")
    public User usuario;

    @SerializedName("error")
    public Error error;

    @SerializedName("pendientes")
    public GenericStatusDate pendientes;

    @SerializedName("finalizadas")
    public GenericStatusDate finalizadas;

    @SerializedName("totales")
    public GenericStatusDate totales;


}
