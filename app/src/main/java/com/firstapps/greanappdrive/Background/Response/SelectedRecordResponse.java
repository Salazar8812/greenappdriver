package com.firstapps.greanappdrive.Background.Response;


import com.firstapps.greanappdrive.Model.Data;
import com.firstapps.greanappdrive.Model.Request;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by icortes on 27/08/17.
 */

public class SelectedRecordResponse {

    @SerializedName("estatus")
    public String status;

    @SerializedName("solicitudes")
    public Aplications request;

    @SerializedName("Recepcion")
    public Aplications Recepcion;

    public static class Aplications {
        @SerializedName("total")
        public String total;

        @SerializedName("per_page")
        public int per_page;

        @SerializedName("current_page")
        public int current_page;

        @SerializedName("next_page_url")
        public int next_page_url;

        @SerializedName("prev_page_url")
        public int prev_page_url;

        @SerializedName("from")
        public int from;

        @SerializedName("to")
        public int to;

        @SerializedName("data")
        public List<Datos> data = new ArrayList<>();

        public static class Datos{
            @SerializedName("id")
            public int id;
            @SerializedName("sol_recoleccion_id")
            public int solRecolectionId;
            @SerializedName("renglon")
            public int line;
            @SerializedName("id_material")
            public int idMaterial;
            @SerializedName("cantidad")
            public String quantity;
            @SerializedName("total")
            public String total;
            @SerializedName("updated_at")
            public String updatedAt;
            @SerializedName("created_at")
            public String createdAt;
            @SerializedName("nombre")
            public String name;
            @SerializedName("url_imagen")
            public String imageURL;
        }
    }

}
