package com.firstapps.greanappdrive.Background;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class ApiCient {
    private static String url="https://firstsoft.com.mx/apigreenapp/public/";
    private static Retrofit retrofit = null;

    public static Retrofit getCliente() {
        Gson gson = new GsonBuilder()
                .create();

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}
