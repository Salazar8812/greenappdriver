package com.firstapps.greanappdrive.Background.Response;

import com.firstapps.greanappdrive.Model.Request;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by icortes on 29/08/17.
 */

public class CollectionResponse {

    @SerializedName("estatus")
    public String status;
    @SerializedName("solicitudes")
    public AplicationsCollection requestCollection;

    public static class AplicationsCollection {

        public Request requestCollection;

        @SerializedName("data")
        public ArrayList<DataCollection> dataCollection = new ArrayList<>();

        public static class DataCollection{
            @SerializedName("id")
            public String id;
            @SerializedName("sol_recoleccion_id")
            public String solRecolectionId;
            @SerializedName("renglon")
            public String line;
            @SerializedName("id_material")
            public String idMaterial;
            @SerializedName("cantidad")
            public String quantity;
            @SerializedName("total")
            public String total;
            @SerializedName("updated_at")
            public String updatedAt;
            @SerializedName("created_at")
            public String createdAt;
            @SerializedName("nombre")
            public String name;
            @SerializedName("url_imagen")
            public String imageURL;
        }
    }
}
