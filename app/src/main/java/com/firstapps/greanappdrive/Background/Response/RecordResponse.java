package com.firstapps.greanappdrive.Background.Response;

import com.firstapps.greanappdrive.Model.Request;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by icortes on 27/08/17.
 */

public class RecordResponse {

    @SerializedName("estatus")
    public String status;
    @SerializedName("solicitudes")
    public Aplications request;

    public static class Aplications {

        public Request request;

        @SerializedName("data")
        public ArrayList<DataRecord> data = new ArrayList<>();

        public static class DataRecord {
            @SerializedName("clave")
            public String key;
            @SerializedName("id_cliente")
            public String idClient;
            @SerializedName("fecha_solicitud")
            public String applicationDate;
            @SerializedName("id_chofer")
            public String idDriver;
            @SerializedName("fecha_recoleccion")
            public String pickupDate;
            @SerializedName("fecha_asignacion")
            public String allocationDate;
            @SerializedName("id_usuario")
            public String userId;
            @SerializedName("total")
            public String total;
            @SerializedName("latitud")
            public String latitud;
            @SerializedName("longitud")
            public String longitud;
            @SerializedName("estatus")
            public String status;
            @SerializedName("nombres")
            public String name;
            @SerializedName("apellidos")
            public String lastName;
            @SerializedName("telefono")
            public String telephone;
            @SerializedName("total_materiales")
            public String totalMaterials;
            @SerializedName("total_kg")
            public String totalKg;
        }
    }
}
