package com.firstapps.greanappdrive.Background;

import com.firstapps.greanappdrive.Background.Request.DateRequest;
import com.firstapps.greanappdrive.Background.Request.LoginRequest;
import com.firstapps.greanappdrive.Background.Request.MarkersRequest;
import com.firstapps.greanappdrive.Background.Request.RecordRequest;
import com.firstapps.greanappdrive.Background.Request.SelectedRecordRequest;
import com.firstapps.greanappdrive.Background.Request.TakAnAppointmentRequest;
import com.firstapps.greanappdrive.Background.Response.DateResponse;
import com.firstapps.greanappdrive.Background.Response.DetailHeaderResponse;
import com.firstapps.greanappdrive.Background.Response.LoginResponse;
import com.firstapps.greanappdrive.Background.Response.MarkersResponse;
import com.firstapps.greanappdrive.Background.Response.RecordResponse;
import com.firstapps.greanappdrive.Background.Response.SelectedRecordResponse;
import com.firstapps.greanappdrive.Background.Response.StoresResponse;
import com.firstapps.greanappdrive.Background.Response.TakAnAppointmentResponse;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.firstapps.greanappdrive.Model.RequestMarkers;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by charlssalazar on 21/08/17.
 */

public interface WebServices{

    @POST("api/auth/login")
    Call<LoginResponse> get_login(@Body LoginRequest request);

    @POST("api/citas3")
    Call<DateResponse> get_dates(@Query("token") String token, @Body DateRequest dateRequest);

    @POST("api/almacenes")
    Call<StoresResponse>  get_stores(@Query("token") String token);

    @POST("api/recolectadas")
    Call<RecordResponse>  get_record(@Query("token") String token, @Body RecordRequest recordRequest);

    @POST("api/sol_detalle")
    Call<SelectedRecordResponse>  get_selected_record(@Query("token") String token, @Query("id_solicitud") String id_solicitud);

    @POST("api/buscar_solicitudes")
    Call<MarkersResponse>  get_solicitudes(@Query("token") String token, @Body MarkersRequest markersRequest);

    @POST("api/tomar_solicitud")
    Call<TakAnAppointmentResponse>  take_request(@Query("token") String token,@Body TakAnAppointmentRequest takAnAppointmentRequest);

    @POST("api/sol_cab_detalle")
    Call<DetailHeaderResponse>  get_headers(@Query("token") String token,@Query("id_solicitud") String id_solicitud );

    @POST("api/tomar_cita")
    Call<TakAnAppointmentResponse>  take_date(@Query("token") String token,@Query("id_cita") String id_cita);

    @POST("api/cancelar_solicitud")
    Call<TakAnAppointmentResponse>  cancel_request(@Query("token") String token,@Body TakAnAppointmentRequest takAnAppointmentRequest);

    @POST("api/recoger_solicitud")
    Call<TakAnAppointmentResponse>  collect_request(@Query("token") String token,@Body TakAnAppointmentRequest takAnAppointmentRequest);

    @POST("api/cancelar_cita")
    Call<TakAnAppointmentResponse>  cancel_date(@Query("token") String token,@Query("id_cita") String id_cita);
}
