package com.firstapps.greanappdrive.Background.Request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 30/08/17.
 */

public class MarkersRequest {
    @SerializedName("latitud")
    public String latitud;

    @SerializedName("longitud")
    public String longitud;

    @SerializedName("distancia")
    public String distancia;
}
