package com.firstapps.greanappdrive.Background.Response;

import com.firstapps.greanappdrive.Model.Date;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by icortes on 23/08/17.
 */

public class DateResponse {

    @SerializedName("estatus")
    public String status;
    @SerializedName("citas")
    public ArrayList<Date> dates = new ArrayList<>();
}
