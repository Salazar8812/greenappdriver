package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class Prices {
    @SerializedName("id")
    public String id;


    @SerializedName("id_material")
    public String id_material;


    @SerializedName("rango_inicial")
    public String rango_inicial;


    @SerializedName("rango_final")
    public String rango_final;


    @SerializedName("precio")
    public String precio;


    @SerializedName("updated_at")
    public String updated_at;


    @SerializedName("created_at")
    public String created_at;
}
