package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class Stores {

    @SerializedName("id")
    public String id;

    @SerializedName("nombre")
    public String nombre;

    @SerializedName("estatus")
    public String estatus;

    @SerializedName("calle")
    public String calle;

    @SerializedName("colonia")
    public String colonia;

    @SerializedName("codigo_postal")
    public String codigo_postal;

    @SerializedName("numero_exterior")
    public String numero_exterior;

    @SerializedName("numero_interior")
    public String numero_interior;

    @SerializedName("id_estado")
    public String id_estado;

    @SerializedName("id_ciudad")
    public String id_ciudad;

    @SerializedName("hora_apertura")
    public String hora_apertura;

    @SerializedName("hora_cierre")
    public String hora_cierre;

    @SerializedName("updated_at")
    private String updated_at;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("tiempo_cita_hora")
    public String tiempo_cita_hora;

    @SerializedName("tiempo_cita_min")
    public String tiempo_cita_min;
}
