package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by icortes on 23/08/17.
 */

public class Date {

    @SerializedName("id")
    public String id;

    @SerializedName("estatus")
    public String estatus;

    @SerializedName("hora_cita")
    public String hora_cita;

    @SerializedName("hora_fin_cita")
    public String hora_fin_cita;

    @SerializedName("id_almacen")
    public String id_almacen;

    @SerializedName("id_chofer")
    public String id_chofer;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("fecha_cita")
    public String fecha_cita;

    @SerializedName("updated_at")
    public String updated_at;
}
