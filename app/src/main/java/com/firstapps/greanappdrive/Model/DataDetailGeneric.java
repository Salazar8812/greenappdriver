package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 02/09/17.
 */

public class DataDetailGeneric {
    @SerializedName("id")
    public String id;

    @SerializedName("sol_recoleccion_id")
    public String sol_recoleccion_id;

    @SerializedName("renglon")
    public String renglon;

    @SerializedName("id_material")
    public String id_material;

    @SerializedName("cantidad")
    public String cantidad;

    @SerializedName("total")
    public String total;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("nombre")
    public String nombre;

    @SerializedName("url_imagen")
    public String url_imagen;
}
