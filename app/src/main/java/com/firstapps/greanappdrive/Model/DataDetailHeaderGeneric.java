package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 02/09/17.
 */

public class DataDetailHeaderGeneric {

    @SerializedName("clave")
    public String clave;

    @SerializedName("id_cliente")
    public String id_cliente;

    @SerializedName("fecha_solicitud")
    public String fecha_solicitud;

    @SerializedName("id_chofer")
    public String id_chofer;

    @SerializedName("fecha_recoleccion")
    public String fecha_recoleccion;

    @SerializedName("fecha_asignacion")
    public String fecha_asignacion;

    @SerializedName("id_usuario")
    public String id_usuario;

    @SerializedName("total")
    public String total;

    @SerializedName("latitud")
    public String latitud;

    @SerializedName("longitud")
    public String longitud;

    @SerializedName("estatus")
    public String estatus;

    @SerializedName("nombres")
    public String nombres;

    @SerializedName("apellidos")
    public String apellidos;

    @SerializedName("telefono")
    public String telefono;

    @SerializedName("nombre_cliente")
    public String nombre_cliente;

    @SerializedName("apellidos_cliente")
    public String apellidos_cliente;

    @SerializedName("telefono_cliente")
    public String telefono_cliente;

    @SerializedName("total_materiales")
    public String total_materiales;

    @SerializedName("total_kg")
    public String total_kg;

    @SerializedName("url_imagen")
    public String url_imagen;


}
