package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class Request {
    @SerializedName("total")
    public String total;

    @SerializedName("per_page")
    public String per_page;


    @SerializedName("current_page")
    public String current_page;


    @SerializedName("last_page")
    public String last_page;


    @SerializedName("next_page")
    public String next_page;


    @SerializedName("prev_page_url")
    public String prev_page_url;


    @SerializedName("from")
    public String from;


    @SerializedName("to")
    public String id;
}
