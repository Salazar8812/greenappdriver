package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class Data {
    @SerializedName("id")
    public String id;

    @SerializedName("id_cliente")
    public String id_cliente;

    @SerializedName("fecha_soicitud")
    public String fecha_soicitud;

    @SerializedName("id_chofer")
    public String id_chofer;

    @SerializedName("fecha_recoleccion")
    public String fecha_recoleccion;

    @SerializedName("fecha_asignacion")
    public String fecha_asignacion;

    @SerializedName("id_usuario")
    public String id_usuario;

    @SerializedName("total")
    public String total;

    @SerializedName("latitud")
    public String latitud;

    @SerializedName("longitud")
    public String longitud;

    @SerializedName("estatus")
    public String estatus;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("nombres")
    public String nombres;

    @SerializedName("apellidos")
    private String apellidos;

    @SerializedName("telefono")
    public String telefono;
}
