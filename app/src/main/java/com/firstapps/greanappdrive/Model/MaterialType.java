package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class MaterialType {
    @SerializedName("id")
    public String id;

    @SerializedName("nombre")
    public String nombre;

    @SerializedName("icono")
    public String icono;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("created_at")
    public String created_at;
}
