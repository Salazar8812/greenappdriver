package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class Materials {

    @SerializedName("id")
    public String id;

    @SerializedName("nombre")
    public String nombre;

    @SerializedName("precios")
    public String precios;


}
