package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class User {
    @SerializedName("id")
    public String id;

    @SerializedName("usuario")
    public String usuario;

    @SerializedName("email")
    public String email;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("updated_at")
    public String updated_at;

    @SerializedName("nombres")
    public String nombres;

    @SerializedName("apellidos")
    public String apellidos;

    @SerializedName("telefono")
    public String telefono;

    @SerializedName("tipo_usuario")
    public String tipo_usuario;

    @SerializedName("url_foto")
    public String url_foto;

    @SerializedName("nombre_empresa")
    public String nombre_empresa;

    @SerializedName("telefono_empresa")
    public String telefono_empresa;

    @SerializedName("foto")
    public String foto;

    @SerializedName("clabe_interbancaria")
    public String clabe_interbancaria;

    @SerializedName("estatus")
    public String estatus;

    @SerializedName("id_estado")
    public String id_estado;

    @SerializedName("nombre_ciudad")
    public String nombre_ciudad;

    @SerializedName("nombre_estado")
    public String nombre_estado;

    @SerializedName("id_ciudad")
    public String id_ciudad;

    @SerializedName("calle")
    public String calle;

    @SerializedName("colonia")
    public String colonia;

    @SerializedName("codigo_postal")
    public String codigo_postal;

    @SerializedName("fecha_baja")
    public String fecha_baja;

    @SerializedName("num_interior")
    public String num_interior;

    @SerializedName("num_exterior")
    public String num_exterior;

    @SerializedName("latitud")
    public String latitud;

    @SerializedName("longitud")
    public String longitud;

    @SerializedName("id_firebase")
    public String id_firebase;

    @SerializedName("responsable")
    public String responsable;

    @SerializedName("fecha_reigistro")
    public String fecha_reigistro;

    @SerializedName("tipo_cliente")
    public String tipo_cliente;

    @SerializedName("nombre_banco")
    public String nombre_banco;
}
