package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class GenericStatusDate {

    @SerializedName("total")
    public int total;

    @SerializedName("dinero")
    public Double dinero;

}
