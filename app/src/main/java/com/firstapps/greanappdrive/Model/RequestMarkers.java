package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 30/08/17.
 */

public class RequestMarkers {
    @SerializedName("clave")
    public String clave;

    @SerializedName("distance")
    public double distance;

    @SerializedName("latitud")
    public String latitud;

    @SerializedName("longitud")
    public String longitud;

    @SerializedName("telefono")
    public String telefono;

    @SerializedName("nombres")
    public String nombres;

    @SerializedName("apellidos")
    public String apellidos;

    @SerializedName("responsable")
    public String responsable;

    @SerializedName("tipo_cliente")
    public String tipo_cliente;

    @SerializedName("total_kg")
    public String total_kg;
}
