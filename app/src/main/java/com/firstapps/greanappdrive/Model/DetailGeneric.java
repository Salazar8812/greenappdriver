package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by charlssalazar on 02/09/17.
 */

public class DetailGeneric {
    @SerializedName("total")
    public String total;

    @SerializedName("per_page")
    public String per_page;

    @SerializedName("current_page")
    public String current_page;

    @SerializedName("last_page")
    public String last_page;

    @SerializedName("next_page_url")
    public String next_page_url;

    @SerializedName("prev_page_url")
    public String prev_page_url;

    @SerializedName("from")
    public String from;

    @SerializedName("to")
    public String to;

    @SerializedName("data")
    public List<DataDetailGeneric> data = new ArrayList<>();
}
