package com.firstapps.greanappdrive.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class Error {
    @SerializedName("message")
    public String message;

    @SerializedName("status_code")
    public String status_code;
}
