package com.firstapps.greanappdrive;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.firstapps.greanappdrive.Adapters.AdapterCollection;
import com.firstapps.greanappdrive.Background.ApiCient;
import com.firstapps.greanappdrive.Background.Request.TakAnAppointmentRequest;
import com.firstapps.greanappdrive.Background.Response.DetailHeaderResponse;
import com.firstapps.greanappdrive.Background.Response.TakAnAppointmentResponse;
import com.firstapps.greanappdrive.Background.WebServices;
import com.firstapps.greanappdrive.CustomLibray.DataPersistent;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.Locale;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by charlssalazar on 02/09/17.
 */

public class DetailCollectionMap extends AppCompatActivity implements View.OnClickListener{
    private String idSolicitud="";
    private DataPersistent dataPersistent;
    private KProgressHUD hud;
    private TextView id_textView;
    private TextView total_textView;
    private TextView name_textView;
    private TextView weight_textView;
    private TextView number_materials_textView;
    private ImageView imgFotoPerfil;
    private AdapterCollection adapterCollection;
    private RecyclerView listCollectionRecyclerView;
    private LinearLayout location_linearLayout;
    private LinearLayout minus_linearLayout;
    private LinearLayout plus_linearLayout;
    private LinearLayout backButton_linearLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_collection_map_activity);
        idSolicitud = getIntent().getExtras().getString("idSolicitud");

        Log.e("id_solcitud",idSolicitud);

        dataPersistent = new DataPersistent(this);
        id_textView = (TextView) findViewById(R.id.id_textView);
        total_textView = (TextView) findViewById(R.id.total_textView);
        name_textView = (TextView) findViewById(R.id.name_textView);
        weight_textView = (TextView) findViewById(R.id.weight_textView);
        number_materials_textView = (TextView) findViewById(R.id.number_materials_textView);
        imgFotoPerfil = (ImageView) findViewById(R.id.imgFotoPerfil);
        listCollectionRecyclerView = (RecyclerView) findViewById(R.id.recycler_record_fragment);

        location_linearLayout = (LinearLayout) findViewById(R.id.location_linearLayout);
        minus_linearLayout = (LinearLayout) findViewById(R.id.minus_linearLayout);
        plus_linearLayout = (LinearLayout) findViewById(R.id.plus_linearLayout);
        backButton_linearLayout = (LinearLayout) findViewById(R.id.backButton_linearLayout);

        listCollectionRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        location_linearLayout.setOnClickListener(this);
        minus_linearLayout.setOnClickListener(this);
        plus_linearLayout.setOnClickListener(this);
        backButton_linearLayout.setOnClickListener(this);

        showProgress();

        getDetailRequest();
    }

    public void showProgress(){
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Espere porfavor")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public void getDetailRequest(){
        WebServices res = ApiCient.getCliente().create(WebServices.class);

        Call<DetailHeaderResponse> call = res.get_headers(dataPersistent.getData(Keys.KEY_TOKEN, ""), idSolicitud);
        call.enqueue(new Callback<DetailHeaderResponse>() {
            @Override
            public void onResponse(Call<DetailHeaderResponse> call, Response<DetailHeaderResponse> response) {
                DetailHeaderResponse detailHeaderResponse = response.body();
                try {
                    if (detailHeaderResponse.estatus.equals("ok")) {
                        adapterCollection = new AdapterCollection(DetailCollectionMap.this,detailHeaderResponse.detalle_solicitud.data);
                        listCollectionRecyclerView.setAdapter(adapterCollection);

                        id_textView.setText(detailHeaderResponse.cabecera_solicitud.data.get(0).clave);
                        total_textView.setText(detailHeaderResponse.cabecera_solicitud.data.get(0).total);
                        name_textView.setText(detailHeaderResponse.cabecera_solicitud.data.get(0).nombre_cliente+" "+detailHeaderResponse.cabecera_solicitud.data.get(0).apellidos_cliente );
                        weight_textView.setText(detailHeaderResponse.cabecera_solicitud.data.get(0).total_kg+"Kg");
                        number_materials_textView.setText(detailHeaderResponse.cabecera_solicitud.data.get(0).total_materiales);
                        Glide.with(DetailCollectionMap.this).load(detailHeaderResponse.cabecera_solicitud.data.get(0).url_imagen).bitmapTransform( new CropCircleTransformation(DetailCollectionMap.this)).into(imgFotoPerfil);

                    } else {
                        Toast.makeText(DetailCollectionMap.this, "No existe informacón detalle", Toast.LENGTH_LONG).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<DetailHeaderResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }


    public void cancelRequest(){
        WebServices res = ApiCient.getCliente().create(WebServices.class);
        TakAnAppointmentRequest takAnAppointmentRequest= new TakAnAppointmentRequest();
        takAnAppointmentRequest.solicitud = idSolicitud;

        Call<TakAnAppointmentResponse> call = res.cancel_request(dataPersistent.getData(Keys.KEY_TOKEN, ""), takAnAppointmentRequest);
        call.enqueue(new Callback<TakAnAppointmentResponse>() {
            @Override
            public void onResponse(Call<TakAnAppointmentResponse> call, Response<TakAnAppointmentResponse> response) {
                TakAnAppointmentResponse detailHeaderResponse = response.body();
                try {
                    if (detailHeaderResponse.status.equals("ok")) {
                        new AlertDialog.Builder(DetailCollectionMap.this)
                                .setTitle("Cancelada")
                                .setMessage("Solicitud cancelada con exito")
                                .setCancelable(false)
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        DetailCollectionMap.this.finish();
                                    }
                                }).show();
                        dataPersistent.saveData(Keys.KEY_ID_SOLCITUD,"");
                    } else {
                        Toast.makeText(DetailCollectionMap.this, "Ocurrio un error al cancelar la solicitud", Toast.LENGTH_LONG).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<TakAnAppointmentResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }


    public void collectRequest(){
        WebServices res = ApiCient.getCliente().create(WebServices.class);
        TakAnAppointmentRequest takAnAppointmentRequest= new TakAnAppointmentRequest();
        takAnAppointmentRequest.solicitud = idSolicitud;

        Call<TakAnAppointmentResponse> call = res.collect_request(dataPersistent.getData(Keys.KEY_TOKEN, ""), takAnAppointmentRequest);
        call.enqueue(new Callback<TakAnAppointmentResponse>() {
            @Override
            public void onResponse(Call<TakAnAppointmentResponse> call, Response<TakAnAppointmentResponse> response) {
                TakAnAppointmentResponse detailHeaderResponse = response.body();
                try {
                    if (detailHeaderResponse.status.equals("ok")) {
                        new AlertDialog.Builder(DetailCollectionMap.this)
                                .setTitle("Recolectada")
                                .setMessage("Solicitud recolectada con exito")
                                .setCancelable(false)
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        DetailCollectionMap.this.finish();
                                    }
                                }).show();
                        dataPersistent.saveData(Keys.KEY_ID_SOLCITUD,"");
                    } else {
                        Toast.makeText(DetailCollectionMap.this, "Ocurrio un error al cancelar la solicitud", Toast.LENGTH_LONG).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                hud.dismiss();
            }

            @Override
            public void onFailure(Call<TakAnAppointmentResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.location_linearLayout:
                new AlertDialog.Builder(DetailCollectionMap.this)
                        .setTitle("" +
                                "Asistencia GPS")
                        .setMessage("¿Desea ejecutar el asistente GPS para ubicar el lugar de la recolección?")
                        .setCancelable(false)
                        .setPositiveButton("Ir a GPS", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                loadRoute();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                break;

            case R.id.minus_linearLayout:
                new AlertDialog.Builder(DetailCollectionMap.this)
                        .setTitle("Cancelar solicitud")
                        .setMessage("Está seguro de cancelar la recolección de la solicitud")
                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                showProgress();
                                cancelRequest();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                break;

            case R.id.plus_linearLayout:

                new AlertDialog.Builder(DetailCollectionMap.this)
                        .setTitle("Recolección de solicitud")
                        .setMessage("Esta seguro de recolectar la solicitud seleccionada")
                        .setCancelable(false)
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                showProgress();
                                collectRequest();
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
                break;

            case R.id.backButton_linearLayout:

                break;
        }
    }



    public void loadRoute(){
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", 12f, 2f, "Where the party is at");
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        try
        {
            startActivity(intent);
        }
        catch(ActivityNotFoundException ex)
        {
            try
            {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            }
            catch(ActivityNotFoundException innerEx)
            {
                Toast.makeText(this, "Please install a maps application", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }
}
