package com.firstapps.greanappdrive;

import android.app.Activity;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firstapps.greanappdrive.Adapters.AdapterMenu;
import com.firstapps.greanappdrive.Fragments.DateFragment;
import com.firstapps.greanappdrive.Fragments.MapCollectionFragment;
import com.firstapps.greanappdrive.Fragments.PerfilFragment;
import com.firstapps.greanappdrive.Fragments.RecordFragment;
import com.firstapps.greanappdrive.CustomLibray.ResideLayout;
import com.firstapps.greanappdrive.Model.MenuItem;
import java.util.ArrayList;

/**
 * Created by charlssalazar on 15/08/17.
 */

public class MainApp extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemClickListener {
    private ListView listMenu_listView;
    private LinearLayout menu_linearLayout;
    private LinearLayout refresh_linearLayout;
    private TextView title_textView;
    private FrameLayout fragmentContent_frameLayout;
    private ResideLayout resideLayout;
    private ArrayList<MenuItem> menu_list = new ArrayList<>();
    private AdapterMenu adapter_menu;
    private FragmentManager FM;
    private LocationManager locationManager;
    public String idMenuOptionSelected="Recolección";
    public static Activity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainapp_activity);

        activity = MainApp.this;

        listMenu_listView = (ListView) findViewById(R.id.listMenu_listView);
        menu_linearLayout = (LinearLayout) findViewById(R.id.menu_linearLayout);
        refresh_linearLayout = (LinearLayout) findViewById(R.id.refresh_linearLayout);
        title_textView = (TextView) findViewById(R.id.title_textView);
        fragmentContent_frameLayout = (FrameLayout) findViewById(R.id.fragmentContent_frameLayout);
        resideLayout = (ResideLayout) findViewById(R.id.reside_layout);

        populateMenu();

        adapter_menu = new AdapterMenu(MainApp.this, menu_list);
        listMenu_listView.setAdapter(adapter_menu);

        listMenu_listView.setOnItemClickListener(this);
        menu_linearLayout.setOnClickListener(this);
        fragmentContent_frameLayout.setOnClickListener(this);
        refresh_linearLayout.setOnClickListener(this);


        gpsCheckStatus();

        loadFragment(new MapCollectionFragment());
    }




    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR2)
    public void loadFragment(Fragment fragment) {
        fragmentContent_frameLayout.removeAllViews();
        FM = getSupportFragmentManager();
        FragmentTransaction FT = FM.beginTransaction();
        FT.replace(R.id.fragmentContent_frameLayout, fragment);
        FT.commit();
    }

    public void populateMenu() {
        MenuItem optionOne = new MenuItem();
        optionOne.iconMenu = R.drawable.ic_leaf_white;
        optionOne.optionMenu = "Recolección";
        menu_list.add(optionOne);

        MenuItem optionTwo = new MenuItem();
        optionTwo.iconMenu = R.drawable.ic_historial;
        optionTwo.optionMenu = "Historial";
        menu_list.add(optionTwo);

        MenuItem optionThree = new MenuItem();
        optionThree.iconMenu = R.drawable.ic_calendar;
        optionThree.optionMenu = "Citas";
        menu_list.add(optionThree);

        MenuItem optionFour = new MenuItem();
        optionFour.iconMenu = R.drawable.ic_user;
        optionFour.optionMenu = "Perfil";
        menu_list.add(optionFour);

        MenuItem optionFive = new MenuItem();
        optionFive.iconMenu = R.drawable.ic_exit;
        optionFive.optionMenu = "Salir";
        menu_list.add(optionFive);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu_linearLayout:
                resideLayout.openPane();
                break;
            case R.id.refresh_linearLayout:
                refreshFragment();
                break;
        }
    }

    public void refreshFragment(){
        switch (idMenuOptionSelected) {
            case "Recolección":
                MapCollectionFragment.get_request_markers();
                Log.e("Refresh","Update Recolección");
                break;
            case "Historial":
                RecordFragment.get_record("1");
                Log.e("Refresh","Update Historial");
                break;

            case "Citas":
                break;

            case "Perfil":
                break;

        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        MenuItem item = (MenuItem) adapterView.getItemAtPosition(i);
        title_textView.setText(item.optionMenu);
        idMenuOptionSelected = item.optionMenu;

        switch (item.optionMenu) {
            case "Recolección":
                resideLayout.closePane();
                loadFragment(new MapCollectionFragment());
                refresh_linearLayout.setVisibility(View.VISIBLE);
                break;
            case "Historial":
                resideLayout.closePane();
                loadFragment(new RecordFragment());
                refresh_linearLayout.setVisibility(View.VISIBLE);
                break;
            case "Citas":
                resideLayout.closePane();
                loadFragment(new DateFragment());
                refresh_linearLayout.setVisibility(View.INVISIBLE);

                break;
            case "Perfil":
                resideLayout.closePane();
                loadFragment(new PerfilFragment());
                refresh_linearLayout.setVisibility(View.INVISIBLE);
                break;
            case "Salir":
                startActivity(new Intent(MainApp.this, Login.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                this.finish();
                break;
        }
    }

    public void gpsCheckStatus(){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("OnActivityReult","Catch Result");
        if(idMenuOptionSelected.equals("Recolección") && requestCode == 0){
            MapCollectionFragment.get_request_markers();
            //Toast.makeText(this,"Por favor presione el boton actualizar para sincronizar la ubicacion",Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onBackPressed() {
        if (resideLayout.isOpen()) {
            resideLayout.closePane();
        } else {
            Login.activity.finish();
            super.onBackPressed();
        }
    }


}
