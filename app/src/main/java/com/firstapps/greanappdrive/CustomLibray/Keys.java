package com.firstapps.greanappdrive.CustomLibray;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class Keys {
    public static String KEY_USER = "usuario";
    public static String KEY_PASSWORD = "password";
    public static String KEY_REMEMBER_ACCOUNT = "remember";
    public static String KEY_TOKEN = "token";
    public static String KEY_TYPE_DIALOG="dialogType";

    public static String KEY_ID_SOLCITUD = "id_solicitud";

    public static String KEY_ID = "id";
    public static String KEY_USER_NICK_NAME = "usuario";
    public static String KEY_EMAIL = "email";
    public static String KEY_NAME = "nombres";
    public static String KEY_LASTNAME = "apellidos";
    public static String KEY_PHONE = "telefono";
    public static String KEY_USER_TYPE = "tipo_usuario";
    public static String KEY_URL_PHOTO = "url_foto";
    public static String KEY_ENTERPRISE_NAME = "nombre_empresa";
    public static String KEY_ENTERPRISE_PHONE = "telefono_empresa";
    public static String KEY_CLABE = "clabe_interbancaria";
    public static String KEY_STATUS = "estatus";
    public static String KEY_ID_STATE = "id_estado";
    public static String KEY_NAME_STATE = "nombre_estado";
    public static String KEY_ID_CITY = "id_ciudad";
    public static String KEY_NAME_CITY = "nombre_ciudad";
    public static String KEY_STREET = "calle";
    public static String KEY_COLONY = "colonia";
    public static String KEY_ZIP_CODE = "codigo_postal";
    public static String KEY_DOWN_DATE = "fecha_baja";
    public static String KEY_NUMBER_INT = "num_interior";
    public static String KEY_NUMBER_EXT = "num_exterior";
    public static String KEY_LAT = "latitud";
    public static String KEY_LON = "longitud";
    public static String KEY_ID_FIREBASE = "id_firebase";
    public static String KEY_LEAD = "responsable";
    public static String KEY_REGISTER_DATE = "fecha_registro";
    public static String KEY_TYPE_CLIENT = "tipo_cliente";
    public static String KEY_BANK_NAME = "nombre_banco";

    public static String KEY_TOTAL_FINISH = "total_finalizadas";
    public static String KEY_TOTAL_PENDING = "total_pendientes";
    public static String KEY_TOTAL_TRANSFER = "total_transferidas";
    public static String KEY_TOTOAL_TRANSFER_MONEY = "total_transferencia_money";


}
