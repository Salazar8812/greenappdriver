package com.firstapps.greanappdrive.CustomLibray;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.firstapps.greanappdrive.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by charlssalazar on 19/08/17.
 */

public class MapInfoLayout implements GoogleMap.InfoWindowAdapter {
    LayoutInflater inflater=null;
    public MapInfoLayout(LayoutInflater inflater) {
        this.inflater=inflater;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return(null);
    }

    @Override
    public View getInfoContents(Marker marker) {
        View myContentView=inflater.inflate(R.layout.map_info_layout_item, null);

        TextView nameEnterprise = ((TextView) myContentView.findViewById(R.id.nameEnterprise_textView));
        TextView distance = ((TextView) myContentView.findViewById(R.id.distance_textView));

        distance.setText(marker.getSnippet());
        nameEnterprise.setText(marker.getTitle());

        return myContentView;
    }

}
