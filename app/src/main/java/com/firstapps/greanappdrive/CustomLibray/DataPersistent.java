package com.firstapps.greanappdrive.CustomLibray;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class DataPersistent {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String data;

    public DataPersistent(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
    }

    public void saveData(String key,String value){
        editor.putString(key,value);
        editor.commit();
    }

    public String getData(String key, String defaultValue){
        return data = sharedPreferences.getString(key, defaultValue);
    }
}
