package com.firstapps.greanappdrive.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firstapps.greanappdrive.Background.Response.CollectionResponse;
import com.firstapps.greanappdrive.Model.DataDetailGeneric;
import com.firstapps.greanappdrive.Model.DetailGeneric;
import com.firstapps.greanappdrive.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by icortes on 29/08/17.
 */

public class AdapterCollection extends RecyclerView.Adapter<AdapterCollection.AdaapterCollectionHolderView> {

    private List<DataDetailGeneric> dataCollections;
    private Activity activity;

    public AdapterCollection(Activity activity,List<DataDetailGeneric> dataCollections) {
        this.dataCollections = dataCollections;
        this.activity = activity;
    }


    @Override
    public AdaapterCollectionHolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(activity).inflate(R.layout.collection_item, parent, false);
        return new AdaapterCollectionHolderView(view);
    }

    @Override
    public void onBindViewHolder(AdaapterCollectionHolderView holder, int position) {
        holder.name_material_textView.setText(dataCollections.get(position).nombre);
        holder.measure_textView.setText(dataCollections.get(position).cantidad+"Kg");
        holder.price_textView.setText("$"+dataCollections.get(position).total);
        Glide.with(activity).load(dataCollections.get(position).url_imagen).into(holder.material_imageView);
    }

    @Override
    public int getItemCount() {
        return dataCollections.size();
    }

    public class AdaapterCollectionHolderView extends RecyclerView.ViewHolder {
        TextView name_material_textView;
        TextView measure_textView;
        TextView price_textView;
        ImageView material_imageView;

        public AdaapterCollectionHolderView(View itemView) {
            super(itemView);
            name_material_textView = (TextView) itemView.findViewById(R.id.name_material_textView);
            measure_textView = (TextView) itemView.findViewById(R.id.measure_textView);
            price_textView = (TextView) itemView.findViewById(R.id.price_textView);
            material_imageView = (ImageView) itemView.findViewById(R.id.material_imageView);
        }
    }
}
