package com.firstapps.greanappdrive.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firstapps.greanappdrive.Background.Response.SelectedRecordResponse;
import com.firstapps.greanappdrive.R;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

/**
 * Created by icortes on 27/08/17.
 */

public class AdapterSelectedRecord extends RecyclerView.Adapter<AdapterSelectedRecord.AdapterSelectedRecordViewHolder> {

    private List<SelectedRecordResponse.Aplications.Datos> date;
    private Activity activity;
    private TextView total_tv;
    private double aux,total;

    public AdapterSelectedRecord(List<SelectedRecordResponse.Aplications.Datos> date,Activity activity,TextView total_tv) {
        this.activity = activity;
        this.date = date;
        this.total_tv = total_tv;
    }

    @Override
    public AdapterSelectedRecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final  View view = LayoutInflater.from(activity).inflate(R.layout.selected_record_item, parent, false);
        return  new AdapterSelectedRecordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterSelectedRecordViewHolder holder, int position) {
        holder.quantity.setText(date.get(position).quantity+"Kg");
        holder.total.setText("$"+date.get(position).total);
        holder.name.setText(date.get(position).name);
        Glide.with(activity).load(R.drawable.reciclado_icono).into(holder.selected_record_image);
        aux = Double.parseDouble(date.get(position).total);
        total = total + aux;
        total_tv.setText("$"+total);
    }

    @Override
    public int getItemCount() {
        return date.size();
    }

    public class AdapterSelectedRecordViewHolder extends RecyclerView.ViewHolder {

        TextView quantity;
        TextView total;
        TextView name;
        ImageView selected_record_image;


        public AdapterSelectedRecordViewHolder(View itemView) {
            super(itemView);

            quantity = (TextView) itemView.findViewById(R.id.selected_record_quantity);
            total = (TextView) itemView.findViewById(R.id.selected_record_total);
            name = (TextView) itemView.findViewById(R.id.selected_record_name);
            selected_record_image= (ImageView) itemView.findViewById(R.id.selected_record_image);
        }
    }
}
