package com.firstapps.greanappdrive.Adapters;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firstapps.greanappdrive.Model.MenuItem;
import com.firstapps.greanappdrive.R;
import java.util.ArrayList;

/**
 * Created by charlssalazar on 18/08/17.
 */

public class AdapterMenu extends BaseAdapter{
    private ArrayList<MenuItem> menu_list= new ArrayList<>();
    private Activity activity;


    public AdapterMenu(Activity activity, ArrayList<MenuItem> menu_list){
        this.activity = activity;
        this.menu_list = menu_list;
    }

    @Override
    public int getCount() {
        return menu_list.size();
    }

    @Override
    public MenuItem getItem(int i) {
        return menu_list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = null;
        TextView optionMenu_textView;
        ImageView iconMenu_imageView;

        if(v == null){
            v= LayoutInflater.from(activity).inflate(R.layout.menu_item,viewGroup,false);
        }

        MenuItem menuItem = getItem(i);
        optionMenu_textView= (TextView)v.findViewById(R.id.optionMenu_textView);
        iconMenu_imageView= (ImageView) v.findViewById(R.id.menuIcon_imageView);

        optionMenu_textView.setText(menuItem.optionMenu);
        try {
            Glide.with(activity).load(menuItem.iconMenu).into(iconMenu_imageView);
            //iconMenu_imageView.setImageResource(menuItem.iconMenu);
            iconMenu_imageView.setColorFilter(Color.argb(255, 255, 255, 255));
        }catch (OutOfMemoryError error){
            error.printStackTrace();
        }
        return v;
    }
}
