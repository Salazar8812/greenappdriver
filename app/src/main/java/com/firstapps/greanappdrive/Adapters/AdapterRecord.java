package com.firstapps.greanappdrive.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firstapps.greanappdrive.Background.Response.RecordResponse;
import com.firstapps.greanappdrive.R;
import com.firstapps.greanappdrive.SelectedRecord;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by icortes on 27/08/17.
 */

public class AdapterRecord extends RecyclerView.Adapter<AdapterRecord.AdapterRecordViewHolder> {

    private ArrayList<RecordResponse.Aplications.DataRecord> dataRecords = new ArrayList<>();
    private Activity activity;

    public AdapterRecord(Activity activity, ArrayList<RecordResponse.Aplications.DataRecord> dataRecords) {
        this.dataRecords = dataRecords;
        this.activity = activity;
    }


    @Override
    public AdapterRecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.record_item, parent, false);
        return new AdapterRecordViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterRecordViewHolder holder, int position) {
        holder.name.setText("Usuario: "+dataRecords.get(position).name +" "+ dataRecords.get(position).lastName);
        holder.idNumber.setText(dataRecords.get(position).key);
        holder.total.setText(String.format("$ %s", dataRecords.get(position).total));
        holder.quantity.setText(String.format("%s Kg", new DecimalFormat("##.##").format(Double.parseDouble(dataRecords.get(position).totalKg))));
        holder.materialsNumber.setText(dataRecords.get(position).totalMaterials);
        holder.telephone.setText(dataRecords.get(position).telephone);
        Glide.with(activity).load(R.drawable.reciclado_icono).into(holder.icon_recycling_imageView);
        holder.icon_recycling_imageView.setColorFilter(Color.argb(255, 255, 255, 255));

        String[] dateSplit;
        dateSplit = dataRecords.get(position).applicationDate.split(" ");
        holder.date.setText(dateSplit[0]);
    }


    @Override
    public int getItemCount() {
        return dataRecords.size();
    }

    public class AdapterRecordViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name;
        TextView idNumber;
        TextView total;
        TextView quantity;
        TextView materialsNumber;
        TextView date;
        TextView telephone;
        ImageView icon_recycling_imageView;

        public AdapterRecordViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.record_name);
            idNumber = (TextView) itemView.findViewById(R.id.record_id_number);
            total = (TextView) itemView.findViewById(R.id.record_total);
            quantity = (TextView) itemView.findViewById(R.id.record_quantity);
            materialsNumber = (TextView) itemView.findViewById(R.id.record_materials_number);
            date = (TextView) itemView.findViewById(R.id.record_date);
            telephone = (TextView) itemView.findViewById(R.id.record_cell_phone);
            icon_recycling_imageView = (ImageView) itemView.findViewById(R.id.icon_recycling_imageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos= getAdapterPosition();
            Log.d("Pos",dataRecords.get(pos).idClient);
            Intent intent = new Intent(activity, SelectedRecord.class);
            intent.putExtra("idSolicitud",dataRecords.get(pos).key);
            activity.startActivity(intent);
        }
    }
}
