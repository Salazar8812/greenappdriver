package com.firstapps.greanappdrive.Adapters;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firstapps.greanappdrive.CustomLibray.Keys;
import com.firstapps.greanappdrive.Dialogs.AdvisoryDates_Dialog;
import com.firstapps.greanappdrive.Model.Date;
import com.firstapps.greanappdrive.Model.SaveDate;
import com.firstapps.greanappdrive.R;
import java.util.ArrayList;

/**
 * Created by icortes on 23/08/17.
 */

public class AdapterDates extends RecyclerView.Adapter<AdapterDates.AdapterDatesViewHolder>{

    private ArrayList<Date> dates;
    private Activity activity;
    public static ImageView iconImage;

    public AdapterDates(Activity activity, ArrayList<Date> dates) {
        this.dates = dates;
        this.activity = activity;
    }

    @Override
    public AdapterDatesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(activity).inflate(R.layout.date_item, parent, false);
        return new AdapterDatesViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(AdapterDatesViewHolder holder, int position) {

        try {
            Glide.with(activity).load(R.drawable.reciclado_icono).into(holder.icon_recycle_imageView);
            holder.dateItemHoraIni.setText(dates.get(position).hora_cita);
            holder.dateItemHoraFin.setText(dates.get(position).hora_fin_cita);
            holder.dateItemEstado.setText(dates.get(position).estatus);
            if (dates.get(position).estatus.equals("A")) {
                holder.content_date_item.setBackground(activity.getResources().getDrawable(R.drawable.fondo_perfil));
                holder.dateItemCheck.setImageResource(R.drawable.ic_add_plus);
                holder.dateItemEstado.setText("DISPONIBLE");

            } else {
                holder.dateItemCheck.setImageResource(R.drawable.ic_minus);
                holder.content_date_item.setBackground(activity.getResources().getDrawable(R.drawable.fondo_perfil_selected));
                holder.dateItemEstado.setText("TU CITA");
            }
        }catch (OutOfMemoryError e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return dates.size();
    }

    public class AdapterDatesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView dateItemHoraIni;
        TextView dateItemHoraFin;
        TextView dateItemEstado;
        ImageView dateItemCheck;
        LinearLayout content_date_item;
        ImageView icon_recycle_imageView;

        public AdapterDatesViewHolder(View itemView) {
            super(itemView);
            dateItemHoraIni = (TextView) itemView.findViewById(R.id.date_item_hora_ini);
            dateItemHoraFin = (TextView) itemView.findViewById(R.id.date_item_hora_fin);
            dateItemEstado = (TextView) itemView.findViewById(R.id.date_item_estado);
            dateItemCheck = (ImageView) itemView.findViewById(R.id.date_item_check);
            content_date_item = (LinearLayout) itemView.findViewById(R.id.content_date_item);
            icon_recycle_imageView = (ImageView) itemView.findViewById(R.id.icon_recycle_imageView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            callDialog(dateItemCheck,pos);
        }
    }

    public void callDialog(ImageView v,int pos){

        if(dates.get(pos).estatus.equals("T") ){
            AdvisoryDates_Dialog advisoryDates_dialog = new AdvisoryDates_Dialog();
            Bundle b = new Bundle();
            b.putInt("position",pos);
            b.putString("titleDialog", "Citas en almacén");
            b.putString("bodyDialog","Esta seguro de cancelar la cita seleccionada");
            b.putString("titleButtonAcceptDialog","Aceptar");
            b.putString("titleButtonCancelDialog", "Cancelar");
            b.putString("typeAction","Cancelar");
            b.putString("idSolcitud",dates.get(pos).id);
            b.putString(Keys.KEY_TYPE_DIALOG,"dateDialog");
            advisoryDates_dialog.setArguments(b);
            advisoryDates_dialog.show(activity.getFragmentManager(),null);
            iconImage = v;
        }else{
            AdvisoryDates_Dialog advisoryDates_dialog = new AdvisoryDates_Dialog();
            Bundle b = new Bundle();
            b.putString("titleDialog", "Citas en almacén");
            b.putString("bodyDialog","Esta seguro de apartar la cita seleccionada");
            b.putString("titleButtonAcceptDialog","Acepatar");
            b.putString("titleButtonCancelDialog", "Cancelar");
            b.putString("idSolcitud",dates.get(pos).id);
            b.putInt("position",pos);
            b.putString("typeAction","Apartar");
            b.putString(Keys.KEY_TYPE_DIALOG,"dateDialog");
            advisoryDates_dialog.setArguments(b);
            advisoryDates_dialog.show(activity.getFragmentManager(),null);
            iconImage = v;
        }
    }
}
