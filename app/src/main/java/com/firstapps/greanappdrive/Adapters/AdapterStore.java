package com.firstapps.greanappdrive.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.firstapps.greanappdrive.Model.Stores;
import com.firstapps.greanappdrive.R;
import java.util.ArrayList;
import java.util.SortedMap;

/**
 * Created by charlssalazar on 23/08/17.
 */

public class AdapterStore extends ArrayAdapter<Stores>{
    private ArrayList<Stores> list_stores = new ArrayList<>();
    private Activity activity;

    public AdapterStore(Context context, int resource, ArrayList<Stores> list_stores, Activity activity) {
        super(context, resource);
        this.list_stores = list_stores;
        this.activity = activity;
    }

    public int getCount(){
        return list_stores.size();
    }

    public Stores getItem(int position){
        return list_stores.get(position);
    }

    public long getItemId(int position){
        return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = null;

        v = LayoutInflater.from(activity).inflate(R.layout.store_spinner_item, parent,
                false);

        TextView store_name = (TextView) v.findViewById(R.id.name_store_textview);

        Stores stores= getItem(position);

        store_name.setText(stores.nombre);

        return v;
    }

    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View v = null;



        v = LayoutInflater.from(activity).inflate(R.layout.store_spinner_item, parent,
                false);

        TextView store_name = (TextView) v.findViewById(R.id.name_store_textview);

        Stores stores= getItem(position);

        store_name.setText(stores.nombre);
        return v;
    }
}